<div align="center">
  <a href="https://gridea.dev">
    <img src="https://gitee.com/xueBingChuan/nodejs-webserve/raw/master/src/static/face.jpg"  width="80px" height="80px">
  </a>
  <h1 align="center">
    WebServe
  </h1>
  <h4 align="center">
   手把手带你创造属于自己的轮子
  </h4>

[comment]: <> ([visit]&#40;https://github.com/xuebingchuan/nodejs-webserve&#41; )

  <a href="javascript:void(0);">
    <img src="https://img.shields.io/github/release/getgridea/gridea.svg?style=flat-square" alt="">
  </a>

  <a href="javascript:void(0);">
    <img src="https://img.shields.io/github/license/getgridea/gridea.svg?style=flat-square" alt="">
  </a>

  <a href="https://github.com/xugaoyi/vuepress-theme-vdoing/actions?query=workflow%3ACI">
    <img src="https://github.com/xugaoyi/vuepress-theme-vdoing/workflows/CI/badge.svg" alt="CI">
  </a>
  <a href="https://github.com/xugaoyi/vuepress-theme-vdoing/actions?query=workflow%3AbaiduPush">
    <img src="https://github.com/xugaoyi/vuepress-theme-vdoing/workflows/baiduPush/badge.svg" alt="baiduPush">
  </a>

</div>

  <h1 align="center">
   项目功能摘要
  </h1>
  1.使用nodejs,参考Express/Koa进行原生搭建,实现了常用的页面托管,上传下载🔥断点传输等功能

  2.博客采用🔥VuePress技术将markdown文档编译为html,再使用vue的模板进行处理

  3.简历网站位于早期使用jq,swiper等技术进行搭建

  4.xue-utile项目是参考vue的🔥源码之后实现了双向绑定,VDom,diff更新,$nextTick等功能(具体内容敬请移步项目仓库)

  5.基于nodejs的在线云盘程序,并支持图片,音频,pdf等文件进行在线预览（预计五月底上线并发布源码！）

  6.等....

  <h3 align="center">
   程序详情
  </h3>
  <div align="center">

  <img src="https://pic.imgdb.cn/item/63db4d93ac6ef8601638e6e1.png"  width="160px" height="80px">

  case1:<a href="http://bing-chuan.work">简历地址<a/>

  </div>
 <div align="center">

  <img src="https://pic.imgdb.cn/item/63db4d9cac6ef8601638f0de.pnn"  width="160px" height="80px">

  case2:
  <a href="http://bing-chuan.work/blog">博客地址<a/>

  </div>
 <div align="center">

  <img src="https://pic.imgdb.cn/item/63db4ccbac6ef860163790cc.jpg"  width="160px" height="80px">

  case3:
  <a href="http://bing-chuan.work/vue2">xue-utile项目<a/>

 </div>



  <h3 align="center">
   用方法介绍
  </h3>

## 项目技术栈
本项目使用到的技术栈如下：
* vite
* `webpack`,gulp
* `vue2`,`vue3`,`vuepress` `react`及其周边
* scss,less,sass
* TypeScript
* 等
## 环境介绍
* node:14.19.1
* 镜像源:https://registry.npmjs.org/


   1.启动nodejs-webserve并使用
  ```bash
  # clone the project
  git clone https://gitee.com/xueBingChuan/nodejs-webserve.git
  
  # enter the project directory
  cd nodejs-webserve
  
  # install dependency
  npm install 
  
  # develop
  node appeach.js 
  ```


   2.xue-utile项目示例在实战项目中引入并使用
  ```bash
  #warehouse address
   https://gitee.com/xueBingChuan/vue-utils

  # Introduce dependency
   npm install xue-vue2
   
  # calling interface
   http://bing-chuan.work/get_cdn/xue-vue2
  
  # remark
  使用前需要先将nodejs-webserve项目启动,通过调用接口(get_cdn/xue-vue2)引入main.js文件,之后可以直接在html页面中使用对应的vue语法
  ```
# 补充
 其他:vue源码项目这样引入的方式是为了方便xue-vue2项目修改之后直接发包,nodejs-webserve项目就直接修改版本号就可以同步xue-vue2的修改内容,类似的实现还有应用到博客中....
